package com.quickapp.ravi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickapp.ravi.duepay.R;
import com.quickapp.ravi.duepay.RequestTransactionActivity;
import com.quickapp.ravi.model.PayRequest;

import java.util.List;

/**
 * Created by ravirane on 10/27/15.
 */
public class PayURecyclerListAdapter extends RecyclerView.Adapter<PayRequestViewHolder> {


    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<PayRequest> payRequestList;
    String parent;
    private Integer bankId = null;



    public PayURecyclerListAdapter(Context context, List<PayRequest> payRequestList, String parent) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.payRequestList = payRequestList;
        this.parent = parent;
    }

    @Override
    public PayRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pay_request_row, parent, false);
        view.setBackgroundResource(mBackground);
        return new PayRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PayRequestViewHolder holder, int position) {
        final PayRequest payRequest = payRequestList.get(position);
        holder.txtRequestId.setText("Request ID : " + Integer.toString(payRequest.requestId));
        holder.txtRequestStatus.setText("Request Status : " + payRequest.requestStatus);
        holder.txtRequestAmount.setText("Amount : " + Double.toString(payRequest.requestAmount) + " " + payRequest.requestCurrency);
        if(payRequest.requestStatus.equalsIgnoreCase("COMPLETED")) {
            holder.txtRequestCompletionDate.setText("Completion Date : " + payRequest.requestCompletionDate);
        } else {
            holder.txtRequestCompletionDate.setText("");
        }
        holder.txtRequestStartDate.setText("Start Date : " + payRequest.requestStartDate);
        holder.txtRequestTitle.setText(payRequest.requestTitle);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, RequestTransactionActivity.class);
                intent.putExtra("payRequest", payRequest);
                intent.putExtra("reqType", "PayU");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return payRequestList.size();
    }
}
