package com.quickapp.ravi.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.quickapp.ravi.duepay.R;

/**
 * Created by ravirane on 10/30/15.
 */
public class CardViewHolder extends RecyclerView.ViewHolder{

    public String mBoundString;

    public final View mView;
    public final TextView txtCardName;
    public final TextView txtBankName;
    public final TextView txtCardNumber;


    public CardViewHolder(View view) {
        super(view);
        mView = view;
        txtCardName = (TextView) view.findViewById(R.id.txtCardName);
        txtBankName = (TextView) view.findViewById(R.id.txtBankName);
        txtCardNumber = (TextView) view.findViewById(R.id.txtCardNumber);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + txtCardNumber.getText();
    }

}
