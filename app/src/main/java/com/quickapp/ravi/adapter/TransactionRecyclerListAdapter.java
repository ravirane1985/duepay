package com.quickapp.ravi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickapp.ravi.duepay.R;
import com.quickapp.ravi.duepay.TransactionDetailsActivity;
import com.quickapp.ravi.model.PayRequest;
import com.quickapp.ravi.model.Transaction;
import com.quickapp.ravi.model.User;

import java.util.List;

/**
 * Created by ravirane on 10/29/15.
 */
public class TransactionRecyclerListAdapter extends RecyclerView.Adapter<TransactionViewHolder> {


    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<Transaction> transactionList;
    private Integer bankId = null;
    private PayRequest payRequest;
    private String reqType;
    private User loginUser;
    private String transactionJson;
    private String parent;


    public TransactionRecyclerListAdapter(Context context, List<Transaction> transactionList, PayRequest payRequest,
                                          String reqType, User loginUser) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.transactionList = transactionList;
        this.payRequest = payRequest;
        this.reqType = reqType;
        this.loginUser = loginUser;
    }

    public TransactionRecyclerListAdapter(Context context, List<Transaction> transactionList, PayRequest payRequest,
                                          String reqType, User loginUser, String jsonResp, String parent) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.transactionList = transactionList;
        this.payRequest = payRequest;
        this.reqType = reqType;
        this.loginUser = loginUser;
        this.transactionJson = jsonResp;
        this.parent = parent;
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_info_row, parent, false);
        view.setBackgroundResource(mBackground);
        return new TransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TransactionViewHolder holder, int position) {
        final Transaction transaction = transactionList.get(position);
        holder.txtTxAmount.setText(transaction.amount + " " + transaction.currency);
        holder.txtTxDate.setText("Last Updated : " + transaction.txStartDate);
        holder.txtTxInfo.setText(getNote(transaction, loginUser));
        holder.txtStatus.setText(transaction.txStatus);
        holder.txtUser.setText(getMessage(transaction,loginUser));


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, TransactionDetailsActivity.class);
                intent.putExtra("transaction", transaction);
                intent.putExtra("payRequest", payRequest);
                intent.putExtra("reqType", reqType);
                intent.putExtra("jsonResp",transactionJson);
                intent.putExtra("parent",parent);
                context.startActivity(intent);
            }
        });
    }

    private String getNote(Transaction transaction, User loginUser) {
        if(transaction.txType.equals("PayU")) {
            return transaction.senderNote;
        } else if(transaction.txType.equals("PayMe")){
            return transaction.receiverNote;
        }
        return "";
    }

    private String getMessage(Transaction transaction, User loginUser) {

        String txMessage = "";
            if(loginUser.uId == transaction.senderUser.uId) {
                txMessage = "You need to pay to " + transaction.receiverUser.uName + "-" + transaction.receiverUser.contactNo;
            } else if(loginUser.uId == transaction.receiverUser.uId){
                txMessage = "You will get paid from "  + transaction.senderUser.uName + "-" + transaction.senderUser.contactNo;
            }
        return txMessage;
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }
}

