package com.quickapp.ravi.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.quickapp.ravi.duepay.R;

/**
 * Created by ravirane on 10/29/15.
 */
public class PayRequestViewHolder extends RecyclerView.ViewHolder {

    public String mBoundString;

    public final View mView;

    public final TextView txtRequestId;
    public final TextView txtRequestTitle;
    public final TextView txtRequestStartDate;
    public final TextView txtRequestAmount;
    public final TextView txtRequestStatus;
    public final TextView txtRequestCompletionDate;


    public PayRequestViewHolder(View view) {
        super(view);
        mView = view;
        txtRequestId = (TextView) view.findViewById(R.id.requestId);
        txtRequestTitle = (TextView) view.findViewById(R.id.requestTitle);
        txtRequestStartDate = (TextView) view.findViewById(R.id.requestStartDate);
        txtRequestAmount = (TextView) view.findViewById(R.id.requestAmount);
        txtRequestStatus = (TextView) view.findViewById(R.id.requestStatus);
        txtRequestCompletionDate = (TextView) view.findViewById(R.id.requestCompletionDate);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + txtRequestId.getText();
    }

}
