package com.quickapp.ravi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickapp.ravi.duepay.CardDetailsActivity;
import com.quickapp.ravi.duepay.R;
import com.quickapp.ravi.model.CreditCard;

import java.util.List;

/**
 * Created by ravirane on 10/30/15.
 */
public class CardRecyclerListAdapter  extends RecyclerView.Adapter<CardViewHolder> {


    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<CreditCard> creditCardList;
    String parent;



    public CardRecyclerListAdapter(Context context, List<CreditCard> creditCardList) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.creditCardList = creditCardList;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_row, parent, false);
        view.setBackgroundResource(mBackground);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, int position) {
        final CreditCard creditCard = creditCardList.get(position);
        holder.txtCardNumber.setText("Card Number : " + creditCard.cardNumber);
        holder.txtBankName.setText("Bank Name : " + creditCard.bankName);
        holder.txtCardName.setText("Card Name : " + creditCard.cardName);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, CardDetailsActivity.class);
                intent.putExtra("creditCard", creditCard);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return creditCardList.size();
    }
}
