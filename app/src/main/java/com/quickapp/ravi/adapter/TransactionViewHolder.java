package com.quickapp.ravi.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.quickapp.ravi.duepay.R;

/**
 * Created by ravirane on 10/28/15.
 */
public class TransactionViewHolder extends RecyclerView.ViewHolder {

    public String mBoundString;

        public final View mView;
        public final TextView txtTxInfo;
        public final TextView txtTxDate;
        public final TextView txtTxAmount;
        public final TextView txtStatus;
        public final TextView txtUser;

        public TransactionViewHolder(View view) {
            super(view);
            mView = view;
            txtTxInfo = (TextView) view.findViewById(R.id.transactionInfo);
            txtTxDate = (TextView) view.findViewById(R.id.transactionDate);
            txtTxAmount = (TextView) view.findViewById(R.id.transactionAmount);
            txtStatus = (TextView) view.findViewById(R.id.transactionState);
            txtUser = (TextView) view.findViewById(R.id.transactionUser);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtTxInfo.getText();
        }

}
