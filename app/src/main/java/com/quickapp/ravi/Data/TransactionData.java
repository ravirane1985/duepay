package com.quickapp.ravi.Data;

import com.quickapp.ravi.model.Transaction;
import com.quickapp.ravi.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ravirane on 10/27/15.
 */
public class TransactionData {

    public static List<Transaction> getTransactions(int reqId) {

        int txId = reqId + 10000;

        List<Transaction> txList = new ArrayList<>();
        Transaction tx = new Transaction(++txId,new User(1,"Ravi","86934154"),"12345678876543","1100.7","Party treat for Diwali",(new Date()).toString(),new User(2,"Ravi","86934154"),"9876654321008","Please see the payment","Accepted", (new Date()).toString(),"SGD");
        txList.add(tx);
        tx = new Transaction(++txId,new User(3,"Ravi","86934154"),"12345678876543","11200.7","Payment for Shopping",(new Date()).toString(),new User(1,"Ravi","86934154"),"9876654321008","Please see the payment","Rejected", (new Date()).toString(),"SGD");
        txList.add(tx);
        tx = new Transaction(++txId,new User(3,"Ravi","86934154"),"12345678876543","11200.7","Payment for Shopping",(new Date()).toString(),new User(1,"Ravi","86934154"),"9876654321008","Please see the payment","Inprogress", (new Date()).toString(),"SGD");
        txList.add(tx);
        tx = new Transaction(++txId,new User(3,"Ravi","86934154"),"12345678876543","11200.7","Payment for Shopping",(new Date()).toString(),new User(1,"Ravi","86934154"),"9876654321008","Please see the payment","Initiated", (new Date()).toString(),"SGD");
        txList.add(tx);
        tx = new Transaction(++txId,new User(3,"Ravi","86934154"),"12345678876543","11200.7","Payment for Shopping",(new Date()).toString(),new User(1,"Ravi","86934154"),"9876654321008","Please see the payment","Rejected", (new Date()).toString(),"SGD");
        txList.add(tx);
        tx = new Transaction(++txId,new User(3,"Ravi","86934154"),"12345678876543","11200.7","Payment for Shopping",(new Date()).toString(),new User(1,"Ravi","86934154"),"9876654321008","Please see the payment","Accepted", (new Date()).toString(),"SGD");
        txList.add(tx);

        return txList;
    }

}
