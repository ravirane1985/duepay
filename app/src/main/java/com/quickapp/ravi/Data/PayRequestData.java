package com.quickapp.ravi.Data;

import com.quickapp.ravi.model.PayRequest;
import com.quickapp.ravi.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ravirane on 10/28/15.
 */
public class PayRequestData {

    public static List<PayRequest> getPayRequests() {
        List<PayRequest> payRequests = new ArrayList<>();

        PayRequest pr = new PayRequest(123,"Pay for Treat","PayU",new Date(),2314,"SGD","Initiated",new Date(),TransactionData.getTransactions(123),new User(1,"Ravi","86934154"));
        payRequests.add(pr);
         pr = new PayRequest(132,"Pay for Treat","PayU",new Date(),2314,"SGD","Initiated",new Date(),TransactionData.getTransactions(132),new User(1,"Ravi","86934154"));
        payRequests.add(pr);
         pr = new PayRequest(145,"Pay for Treat","PayMe",new Date(),2314,"SGD","Initiated",new Date(),TransactionData.getTransactions(145),new User(1,"Ravi","86934154"));
        payRequests.add(pr);
         pr = new PayRequest(234,"Pay for Treat","PayMe",new Date(),2314,"SGD","Initiated",new Date(),TransactionData.getTransactions(234),new User(1,"Ravi","86934154"));
        payRequests.add(pr);
         pr = new PayRequest(156,"Pay for Treat","PayU",new Date(),2314,"SGD","Initiated",new Date(),TransactionData.getTransactions(156),new User(1,"Ravi","86934154"));
        payRequests.add(pr);
         pr = new PayRequest(456,"Pay for Treat","PayMe",new Date(),2314,"SGD","Initiated",new Date(),TransactionData.getTransactions(456),new User(1,"Ravi","86934154"));
        payRequests.add(pr);
         pr = new PayRequest(231,"Pay for Treat","PayU",new Date(),2314,"SGD","Initiated",new Date(),TransactionData.getTransactions(231),new User(1,"Ravi","86934154"));
        payRequests.add(pr);

        return payRequests;
    }
}
