package com.quickapp.ravi.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.quickapp.ravi.model.CreditCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 10/30/15.
 */
public class CreditCardDao  extends BaseDao  {




    private static String tableName = "CREDITCARD";

    public CreditCardDao(Context context) {
        super(context);
    }


    public void populateBankDb() {

    }

    public static void saveCard(CreditCard creditCard) {
            ContentValues values = new ContentValues();
            values.put("cardNumber",creditCard.cardNumber);
            values.put("cardName",creditCard.cardName);
            values.put("bankName",creditCard.bankName);
            values.put("cardExpiry",creditCard.cardExpiry);
            values.put("cvv",creditCard.cvv);
            db.insert(tableName,null,values);
    }

    public static void clearCards() {
        String deletQuery = "DELETE FROM CREDITCARD";
        db.execSQL(deletQuery);
    }

    public static void deleteCard(String creditCardNo) {
       if(creditCardNo != null) {
           String deletQuery = "DELETE FROM CREDITCARD WHERE cardNumber = '" + creditCardNo + "'" ;
           db.execSQL(deletQuery);
       }

    }

    public static  List<CreditCard> getCards(Context context) {
        open(context);
        List<CreditCard> creditCards = new ArrayList<>();

        Cursor mCursor = db.query(tableName,null , null,null,
                null, null, null , null);

        if (mCursor.moveToFirst()) {
            do {
                CreditCard creditCard = new CreditCard();
                creditCard.cardExpiry = mCursor.getString(mCursor.getColumnIndexOrThrow("cardExpiry"));
                creditCard.cardName = mCursor.getString(mCursor.getColumnIndexOrThrow("cardName"));
                creditCard.cardNumber =  mCursor.getString(mCursor.getColumnIndexOrThrow("cardNumber"));
                creditCard.bankName =  mCursor.getString(mCursor.getColumnIndexOrThrow("bankName"));
                creditCard.cvv = mCursor.getString(mCursor.getColumnIndexOrThrow("cvv"));
                creditCards.add(creditCard);
            } while (mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return creditCards;
    }

}
