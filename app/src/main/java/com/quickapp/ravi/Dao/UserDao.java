package com.quickapp.ravi.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.quickapp.ravi.model.User;

/**
 * Created by ravirane on 10/30/15.
 */
public class UserDao extends BaseDao {

    private static String userTable = "USER";

    public UserDao(Context context) {
        super(context);
    }

    public static void saveUser(User user) {
            ContentValues values = new ContentValues();
            values.put("userName",user.uName );
            values.put("contactNo",user.contactNo);
            db.insert(userTable,null,values);
    }

    public static void clearUser() {
        String deletQuery = "DELETE FROM USER";
        db.execSQL(deletQuery);
    }

    public static  User getUser(Context context) {
        open(context);
        User user = null;
        Cursor mCursor = db.query(userTable,null , null,null,
                null, null, null , null);
        if (mCursor.moveToFirst()) {
                user = new User();
                user.uName = mCursor.getString(mCursor.getColumnIndexOrThrow("userName"));
                user.contactNo = mCursor.getString(mCursor.getColumnIndexOrThrow("contactNo"));
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return user;
    }

}
