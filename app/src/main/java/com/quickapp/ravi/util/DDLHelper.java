package com.quickapp.ravi.util;

import android.content.Context;

import com.quickapp.ravi.Dao.BaseDao;

/**
 * Created by ravirane on 8/16/15.
 */
public class DDLHelper extends BaseDao {

    public DDLHelper(Context context) {
        super(context);
    }

    public static void createUserTable(Context context) {
        open(context);
        String profile = "CREATE TABLE IF NOT EXISTS USER( userName TEXT, contactNo TEXT )";
        db.execSQL(profile);
        close();
    }

    public static void createCreditCard(Context context) {
        open(context);
        String profile = "CREATE TABLE IF NOT EXISTS CREDITCARD( cardNumber TEXT, cardName TEXT, bankName TEXT, cardExpiry TEXT, cvv TEXT )";
        db.execSQL(profile);
        close();
    }
}
