package com.quickapp.ravi.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String dbName = "duePay.db";

    public DatabaseHelper(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS PROFILE");
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

      //  db.execSQL(getProfileDDL());
    }

    public String getProfileDDL() {
        return "CREATE TABLE IF NOT EXISTS PROFILE( PROFILENAME TEXT PRIMARY KEY, EMAIL TEXT, AGE INTEGER, INCOME TEXT )";
    }

}
