package com.quickapp.ravi.parse;

import com.quickapp.ravi.model.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ravirane on 10/31/15.
 */
public class ParseUser {

    public static Map<Integer,User> getUserMap(String userJson) {
        Map<Integer,User> userMap = new HashMap<>();
        try {
            JSONObject jsonRootObject = new JSONObject(userJson);
            JSONArray jsonArray = jsonRootObject.optJSONArray("user");
            for(int i = 0; i < jsonArray.length(); i++ ) {
                User user = getUser(jsonArray.getJSONObject(i));
                userMap.put(user.uId,user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userMap;
    }

    public static User getUser(String userJson) {
        User user = null;
        try {
            JSONObject jsonRootObject = new JSONObject(userJson);
            JSONObject job = jsonRootObject.getJSONObject("user");
            user = getUser(job);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public static User getUser(JSONObject job) {
        User user = new User();

        if(job.optString("_id") != null) {
            user.uId = job.optInt("_id");
        }
        if(job.optString("userName") != null) {
            user.uName = job.optString("userName");
        }
        if(job.optString("phoneNo") != null) {
            user.contactNo = job.optString("phoneNo");
        }
        if(job.optString("country") != null) {
            user.country = job.optString("country");
        }
        if(job.optString("phoneAuth") != null) {
            user.phoneVerified = job.optBoolean("phoneAuth");
        }
        if(job.optString("mailAuth") != null) {
            user.emailVerified = job.optBoolean("mailAuth");
        }

        return user;
    }

}
