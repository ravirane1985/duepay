package com.quickapp.ravi.parse;

import com.quickapp.ravi.model.Transaction;
import com.quickapp.ravi.model.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ravirane on 10/31/15.
 */
public class ParseTransaction {

    public static Map<Integer,User> userMap;

    public static List<Transaction> getTransactions(String transactionJson) {
         List<Transaction> transactionList = new ArrayList<>();
            try {
                JSONObject jsonRootObject = new JSONObject(transactionJson);
                JSONArray jsonArray = jsonRootObject.optJSONArray("transaction");
                for(int i = 0; i < jsonArray.length(); i++ ) {
                   Transaction transaction = getTransaction(jsonArray.getJSONObject(i));
                    transactionList.add(transaction);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

         return transactionList;
    }

    public static Transaction getTransaction(JSONObject job) {
        Transaction transaction = new Transaction();
        if(job.optString("_id") != null) {
            transaction.transactionId = job.optInt("_id");
        }
        if(job.optString("requestId") != null) {
            transaction.requestId = job.optInt("requestId");
        }
        if(job.optString("senderId") != null) {
            transaction.senderUser = userMap.get(job.optInt("senderId"));
        }
        if(job.optString("senderCardNo") != null) {
            transaction.senderCardNo = job.optString("senderCardNo");
        }
        if(job.optString("senderNote") != null) {
            transaction.senderNote = job.optString("senderNote");
        }
        if(job.optString("initiateDate") != null) {
            transaction.txStartDate = job.optString("initiateDate");
        }
        if(job.optString("currency") != null) {
            transaction.currency = job.optString("currency");
        }
        if(job.optString("amount") != null) {
            transaction.amount = job.optString("amount");
        }
        if(job.optString("transactionType") != null) {
            transaction.txType = job.optString("transactionType");
        }
        if(job.optString("receiverId") != null) {
            transaction.receiverUser = userMap.get(job.optInt("receiverId"));
        }
        if(job.optString("receiverCardNo") != null) {
            transaction.receiverCardNo = job.optString("receiverCardNo");
        }
        if(job.optString("receiverNote") != null) {
            transaction.currency = job.optString("receiverNote");
        }
        if(job.optString("completionDate") != null) {
            transaction.txCompletionDate = job.optString("completionDate");
        }
        if(job.optString("payStatus") != null) {
            transaction.payStatus = job.optString("payStatus");
        }
        if(job.optString("payResponse") != null) {
            transaction.payResponse = job.optString("payResponse");
        }
        if(job.optString("status") != null) {
            transaction.txStatus = job.optString("status");
        }
        return transaction;
    }

}
