package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.quickapp.ravi.model.User;


public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {

                                                   User user = new User();

                                                   EditText txtName = (EditText) findViewById(R.id.txtProfileName);
                                                   user.uName = txtName.getText().toString();
                                                   EditText txtMailId = (EditText) findViewById(R.id.txtMailId);
                                                   user.mailId = txtMailId.getText().toString();
                                                   EditText txtCountryCode = (EditText) findViewById(R.id.txtCountryCode);
                                                   user.countryCode = txtCountryCode.getText().toString();
                                                   EditText txtPhoneNo = (EditText) findViewById(R.id.txtPhoneNo);
                                                   user.contactNo = txtPhoneNo.getText().toString();
                                                   Intent intent = new Intent(Register.this, Verify.class);
                                                   intent.putExtra("user",user);
                                                   Register.this.startActivity(intent);
                                               }
                                           }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
