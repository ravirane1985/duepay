package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.quickapp.ravi.Dao.CreditCardDao;
import com.quickapp.ravi.model.CreditCard;


public class ActivityAddCard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_add_card);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Button btnSaveCard = (Button) findViewById(R.id.btnSaveCard);
        btnSaveCard.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               CreditCard creditCard = new CreditCard();
                                               EditText txtAddBankName = (EditText) findViewById(R.id.txtAddBankName);
                                               creditCard.bankName = txtAddBankName.getText().toString();
                                               EditText txtAddCardName = (EditText) findViewById(R.id.txtAddCardName);
                                               creditCard.cardName = txtAddCardName.getText().toString();
                                               EditText txtAddCardNo = (EditText) findViewById(R.id.txtAddCardNo);
                                               creditCard.cardNumber = txtAddCardNo.getText().toString();
                                               EditText txtAddCVV = (EditText) findViewById(R.id.txtAddCVV);
                                               creditCard.cvv = txtAddCVV.getText().toString();
                                               EditText txtAddExpiry = (EditText) findViewById(R.id.txtAddExpiry);
                                               creditCard.cardExpiry = txtAddExpiry.getText().toString();
                                               CreditCardDao.open(ActivityAddCard.this);
                                               CreditCardDao.saveCard(creditCard);
                                               CreditCardDao.close();
                                               Intent intent = new Intent(ActivityAddCard.this, ActivityWalletCard.class);
                                               ActivityAddCard.this.startActivity(intent);
                                           }
                                       }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_add_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            Intent intent = new Intent(ActivityAddCard.this, ActivityWalletCard.class);
            ActivityAddCard.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
