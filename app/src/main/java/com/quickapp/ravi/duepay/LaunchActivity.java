package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.quickapp.ravi.Dao.UserDao;
import com.quickapp.ravi.model.User;
import com.quickapp.ravi.parse.ParseUser;
import com.quickapp.ravi.util.DDLHelper;
import com.quickapp.ravi.util.HTTPConnect;
import com.quickapp.ravi.util.MyAppApplication;

import java.util.Map;


public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        DDLHelper.createUserTable(getApplicationContext());
        User user = UserDao.getUser(LaunchActivity.this);
        if(user == null ) {
            Intent intent = new Intent(LaunchActivity.this, Register.class);
            LaunchActivity.this.startActivity(intent);
        } else {
            new AppLoaderTask().execute(user.contactNo);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_launch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class AppLoaderTask extends AsyncTask<String, Integer, Void> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(String... vals) {

            String url = "http://ravis-mbp:8000/user/contact/" + vals[0].trim();

            new HttpAsyncTaskGetLoginUser().execute(url);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            DDLHelper.createCreditCard(getApplicationContext());
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            Intent intent = new Intent(LaunchActivity.this, PayDashboard.class);
            intent.putExtra("tabNo", 0);
            LaunchActivity.this.startActivity(intent);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }

    private class HttpAsyncTaskGetAllUser extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... urls) {
            HTTPConnect connect = new HTTPConnect();
            String jsonResp = connect.GET(urls[0]);
            Map<Integer,User> userMap =  ParseUser.getUserMap(jsonResp);
            MyAppApplication mApp = ((MyAppApplication) getApplicationContext());
            mApp.usersMap = userMap;
            return null;
        }


    }

    private class HttpAsyncTaskGetLoginUser extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... urls) {
            HTTPConnect connect = new HTTPConnect();
            String jsonResp = connect.GET(urls[0]);
            User user =  ParseUser.getUser(jsonResp);
            MyAppApplication mApp = ((MyAppApplication) getApplicationContext());
            mApp.loginUser = user;
            new HttpAsyncTaskGetAllUser().execute("http://ravis-mbp:8000/user/all");
            return null;
        }


    }
}
