package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.quickapp.ravi.Dao.CreditCardDao;
import com.quickapp.ravi.model.CreditCard;
import com.quickapp.ravi.model.PayRequest;
import com.quickapp.ravi.model.Transaction;
import com.quickapp.ravi.parse.CreateJson;

import java.util.List;


public class TransactionDetailsActivity extends AppCompatActivity {

    PayRequest payRequest;
    private String reqType;
    private String transactionJson;
    private String parent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        payRequest = (PayRequest) intent.getSerializableExtra("payRequest");
        final Transaction transaction = (Transaction) intent.getSerializableExtra("transaction");
        reqType = intent.getStringExtra("reqType");
        transactionJson = intent.getStringExtra("jsonResp");
        parent = intent.getStringExtra("parent");

        if(transaction != null) {
            TextView lblTxId = (TextView) findViewById(R.id.lblTxId);
            lblTxId.setText("Transaction ID : " + Integer.toString(transaction.transactionId));
            TextView lblTxEndDate = (TextView) findViewById(R.id.lblTxEndDate);
            lblTxEndDate.setText("Completion Date : " + transaction.txCompletionDate.toString());
            TextView lblTxAmount = (TextView) findViewById(R.id.lblTxAmount);
            lblTxAmount.setText("Amount : " + transaction.amount + " " + transaction.currency);
            TextView lblTxReceiver = (TextView) findViewById(R.id.lblTxReceiver);
            lblTxReceiver.setText("Receiver : " + transaction.receiverUser.uName + " " + transaction.receiverUser.contactNo);
            TextView lblTxStatus = (TextView) findViewById(R.id.lblTxStatus);
            TextView lblTxReceiverNote = (TextView) findViewById(R.id.lblTxReceiverNote);
            lblTxReceiverNote.setText("Receiver Note : " + transaction.receiverNote);
            lblTxStatus.setText("Status : " + transaction.txStatus);
            TextView lblTxStartDate = (TextView) findViewById(R.id.lblTxStartDate);
            lblTxStartDate.setText("Transaction Start Date : " + transaction.txStartDate.toString());
            TextView lblTxSender = (TextView) findViewById(R.id.lblTxSenderName);
            lblTxSender.setText("Sender : " + transaction.senderUser.uName + " " + transaction.senderUser.contactNo);
            TextView lblTxSenderNote = (TextView) findViewById(R.id.lblTxSenderNote);
            lblTxSenderNote.setText("Sender Note : " + transaction.senderNote);
            TextView lblTxPayStatus = (TextView) findViewById(R.id.lblPayStatus);
            lblTxPayStatus.setText("Payment Status : " + transaction.payStatus);
        }

        if("payreq".equals(parent)) {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.bottomLayoutAccept);
            linearLayout.setVisibility(View.VISIBLE);
            linearLayout = (LinearLayout) findViewById(R.id.bottomLayoutCancel);
            linearLayout.setVisibility(View.INVISIBLE);
        }  else {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.bottomLayoutAccept);
            linearLayout.setVisibility(View.INVISIBLE);
            linearLayout = (LinearLayout) findViewById(R.id.bottomLayoutCancel);
            linearLayout.setVisibility(View.VISIBLE);
        }

        List<CreditCard> creditCardList = CreditCardDao.getCards(TransactionDetailsActivity.this);
        CreditCardDao.close();
        String[] items = new String[creditCardList.size()+1];
        int i = 0;
        items[i] = "Select Card";
        i++;
        for(CreditCard creditCard: creditCardList) {
            items[i] = creditCard.cardNumber; i++;
        }
        Spinner dropdown = (Spinner)findViewById(R.id.spnrCardSelect);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        dropdown.setAdapter(adapter);


        Button btnAccept = (Button) findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            EditText txtSubmitNote = (EditText) findViewById(R.id.txtTxSubmitNote);
                                            String submitNote = txtSubmitNote.getText().toString();
                                            Spinner spnrCardSelect = (Spinner) findViewById(R.id.spnrCardSelect);
                                            String cardNo = spnrCardSelect.getSelectedItem().toString();
                                            String status = "Accepted";
                                            String txType = transaction.txType;
                                            String reqJson = CreateJson.getUpdateTxJson();
                                        }
                                    }
        );


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_transaction_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            Intent intent = new Intent(TransactionDetailsActivity.this, RequestTransactionActivity.class);
            intent.putExtra("payRequest",payRequest);
            intent.putExtra("reqType",reqType);
            intent.putExtra("jsonResp",transactionJson);
            intent.putExtra("parent",parent);
            TransactionDetailsActivity.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
