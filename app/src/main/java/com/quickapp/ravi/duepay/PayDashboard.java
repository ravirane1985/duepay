package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.quickapp.ravi.Dao.UserDao;
import com.quickapp.ravi.adapter.PayFragementAdapter;
import com.quickapp.ravi.util.HTTPConnect;


public class PayDashboard extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        PayFragementAdapter payFragementAdapter = null;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_dashboard);

        Intent intent = getIntent();
        int tabNo = intent.getIntExtra("tabNo", 0);
        String drawerState = intent.getStringExtra("drawerState");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);


         /* Navigation item*/

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked())
                    menuItem.setChecked(false);
                else menuItem.setChecked(true);
                Log.e("Inside Menu", "Inside Menu");
                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                  /*  case R.id.nav_profile:
                        Intent intent = new Intent(PayDashboard.this, ProfileActivity.class);
                        FIDashBoardActivity.this.startActivity(intent);
                        return true; */
                    case R.id.nav_payreq:
                        //new HttpAsyncTaskGetPayReq().execute("http://localhost:8000/transaction/payreq/all/2");
                        new HttpAsyncTaskGetPayReq().execute("http://ravis-mbp:8000/transaction/payreq/all/2");
                        return true;
                    case R.id.nav_wallet:
                        Intent intent = new Intent(PayDashboard.this, ActivityWalletCard.class);
                        PayDashboard.this.startActivity(intent);
                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Default Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });


         /*Drawer layout*/

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            payFragementAdapter = setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(tabNo);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(payFragementAdapter.getTabView(i));
        }

        if(drawerState != null && drawerState.equals("open")) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }

    }


    private PayFragementAdapter setupViewPager(ViewPager viewPager) {
        PayFragementAdapter adapter = new PayFragementAdapter(getSupportFragmentManager(),PayDashboard.this);
        adapter.addFragment(PayMeFragement.newInstance("PayMe"), "PayMe");
        adapter.addFragment(PayUFragement.newInstance("PayU"), "PayU");
        adapter.addFragment(InShopFragement.newInstance("PayBiz"), "PayBiz");
        viewPager.setAdapter(adapter);
        return adapter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pay_dashboard, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            //noinspection SimplifiableIfStatement
            case R.id.action_settings:
                finish();
                System.exit(0);


            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_add : {
                Intent intent = new Intent(PayDashboard.this, CreateRequestActivity.class);
                PayDashboard.this.startActivity(intent);
                break;
            }

            case R.id.action_clear:
                UserDao.open(PayDashboard.this);
                UserDao.clearUser();
                UserDao.close();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class HttpAsyncTaskGetPayReq extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            HTTPConnect connect = new HTTPConnect();
            String jsonResp = connect.GET(urls[0]);

            Intent intent = new Intent(PayDashboard.this, RequestTransactionActivity.class);
            intent.putExtra("parent", "payreq");
            intent.putExtra("jsonResp", jsonResp);
            Log.e(jsonResp, jsonResp);
            PayDashboard.this.startActivity(intent);
            return null;
        }
    }
}
