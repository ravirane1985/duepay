package com.quickapp.ravi.duepay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickapp.ravi.Data.PayRequestData;
import com.quickapp.ravi.adapter.PayURecyclerListAdapter;

/**
 * Created by ravirane on 10/27/15.
 */
public class PayUFragement  extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    public PayUFragement() {
        super();
    }

    public static PayUFragement newInstance(String str) {
        PayUFragement payUFragement = new PayUFragement();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_PARAM1, str);
        payUFragement.setArguments(bundle);
        return payUFragement;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView rv = (RecyclerView) inflater.inflate(
                R.layout.fragment_pay_u, container, false);
        setupRecyclerView(rv);
        return rv;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new PayURecyclerListAdapter(getActivity(),
                PayRequestData.getPayRequests(), "payUFragement"));
    }

}
