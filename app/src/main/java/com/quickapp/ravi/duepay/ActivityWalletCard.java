package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.quickapp.ravi.Dao.CreditCardDao;
import com.quickapp.ravi.adapter.CardRecyclerListAdapter;


public class ActivityWalletCard extends AppCompatActivity {

    private CardRecyclerListAdapter cardRecyclerListAdapter;
    private RecyclerView recyclerviewWalletCard;
    private  String reqType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_wallet_card);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerviewWalletCard = (RecyclerView) findViewById(R.id.recyclerviewWalletCard);
        recyclerviewWalletCard.setLayoutManager(new LinearLayoutManager(this));
        cardRecyclerListAdapter = new CardRecyclerListAdapter(ActivityWalletCard.this,
                CreditCardDao.getCards(ActivityWalletCard.this));
        recyclerviewWalletCard.setAdapter(cardRecyclerListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_wallet_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_add) {
            Intent intent = new Intent(ActivityWalletCard.this, ActivityAddCard.class);
            ActivityWalletCard.this.startActivity(intent);
        }
        if (id == android.R.id.home) {
            Intent intent = new Intent(ActivityWalletCard.this, PayDashboard.class);
            ActivityWalletCard.this.startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
