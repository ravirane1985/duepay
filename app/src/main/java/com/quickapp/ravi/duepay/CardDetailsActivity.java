package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quickapp.ravi.Dao.CreditCardDao;
import com.quickapp.ravi.model.CreditCard;


public class CardDetailsActivity extends AppCompatActivity {

    private CreditCard creditCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        creditCard = (CreditCard) intent.getSerializableExtra("creditCard");


        if(creditCard != null) {
            TextView lblViewBankName = (TextView) findViewById(R.id.lblViewBankName);
            lblViewBankName.setText(creditCard.bankName);
            TextView lblViewCardName = (TextView) findViewById(R.id.lblViewCardName);
            lblViewCardName.setText(creditCard.cardName);
            TextView lblViewCardNo = (TextView) findViewById(R.id.lblViewCardNo);
            lblViewCardNo.setText(creditCard.cardNumber);
            TextView lblViewCVV = (TextView) findViewById(R.id.lblViewCVV);
            lblViewCVV.setText(creditCard.cvv);
            TextView lblViewExpiry = (TextView) findViewById(R.id.lblViewExpiry);
            lblViewExpiry.setText(creditCard.cardExpiry);
        }

        Button btnRemoveCard = (Button) findViewById(R.id.btnRemoveCard);
        btnRemoveCard.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 TextView lblViewCardNo = (TextView) findViewById(R.id.lblViewCardNo);
                                                 String cardNo = lblViewCardNo.getText().toString();
                                                 CreditCardDao.open(CardDetailsActivity.this);
                                                 CreditCardDao.deleteCard(cardNo);
                                                 CreditCardDao.close();
                                                 Intent intent = new Intent(CardDetailsActivity.this, ActivityWalletCard.class);
                                                 CardDetailsActivity.this.startActivity(intent);
                                             }
                                         }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            Intent intent = new Intent(CardDetailsActivity.this, ActivityWalletCard.class);
            CardDetailsActivity.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
