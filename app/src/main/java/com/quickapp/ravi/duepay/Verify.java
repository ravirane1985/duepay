package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.quickapp.ravi.Dao.UserDao;
import com.quickapp.ravi.model.User;
import com.quickapp.ravi.parse.CreateJson;
import com.quickapp.ravi.util.HTTPConnect;


public class Verify extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        Intent intent = getIntent();
        final User user = (User) intent.getSerializableExtra("user");

        Button btnVerifyMail = (Button) findViewById(R.id.btnVerifyMail);
        btnVerifyMail.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               EditText txtName = (EditText) findViewById(R.id.txtOtpMail);
                                               if(txtName.getText().toString().equals("1234")) {
                                                   user.emailVerified = true;
                                               }
                                               if(user.emailVerified == true && user.phoneVerified == true) {
                                                   //save user rest call
                                                   UserDao.open(Verify.this);
                                                   UserDao.saveUser(user);
                                                   UserDao.close();
                                                   Intent intent = new Intent(Verify.this, LaunchActivity.class);
                                                   Verify.this.startActivity(intent);
                                               }
                                           }
                                       }
        );

        Button btnVerifyPhone = (Button) findViewById(R.id.btnVerifyPhone);
        btnVerifyPhone.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 EditText txtName = (EditText) findViewById(R.id.txtOtpPhone);
                                                 if(txtName.getText().toString().equals("1234")) {
                                                     user.phoneVerified = true;
                                                 }
                                                 if(user.emailVerified == true && user.phoneVerified == true) {
                                                     //save user rest call
                                                     UserDao.open(Verify.this);
                                                     UserDao.saveUser(user);
                                                     UserDao.close();
                                                     String userJson = CreateJson.getUserJson(user);
                                                     new HttpAsyncTaskCreateUser().execute("http://ravis-mbp:8000/user/", userJson);
                                                 }
                                             }
                                         }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_verify, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class HttpAsyncTaskCreateUser extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... data) {
            HTTPConnect connect = new HTTPConnect();
            connect.POST(data[0],data[1]);
            Intent intent = new Intent(Verify.this, LaunchActivity.class);
            Verify.this.startActivity(intent);
            return null;
        }
    }
}
