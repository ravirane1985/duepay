package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.quickapp.ravi.adapter.TransactionRecyclerListAdapter;
import com.quickapp.ravi.model.PayRequest;
import com.quickapp.ravi.model.Transaction;
import com.quickapp.ravi.model.User;
import com.quickapp.ravi.parse.ParseTransaction;
import com.quickapp.ravi.util.MyAppApplication;

import java.util.List;


public class RequestTransactionActivity  extends AppCompatActivity {

    private TransactionRecyclerListAdapter transactionRecyclerListAdapter;
    private RecyclerView reqTxView;
    private  String reqType;
    private String parent;
    private String transactionJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyAppApplication mApp = ((MyAppApplication) getApplicationContext());
        User loginUser = mApp.loginUser;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_transaction);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        final PayRequest payRequest = (PayRequest) intent.getSerializableExtra("payRequest");
        parent= intent.getStringExtra("parent");
        reqType = intent.getStringExtra("reqType");

        reqTxView = (RecyclerView) findViewById(R.id.recyclerviewReqTx);
        reqTxView.setLayoutManager(new LinearLayoutManager(this));

        if(parent != null && parent.equals("payreq")) {
            String resp = intent.getStringExtra("jsonResp");
            if(resp != null) {
                transactionJson = resp;
            }
            ParseTransaction.userMap = mApp.usersMap;
            List<Transaction> transactionList = ParseTransaction.getTransactions(transactionJson);
            transactionRecyclerListAdapter = new TransactionRecyclerListAdapter(RequestTransactionActivity.this,
                    transactionList,payRequest,reqType,loginUser,transactionJson,parent);
        } else {
            transactionRecyclerListAdapter = new TransactionRecyclerListAdapter(RequestTransactionActivity.this,
                    payRequest.transactionList,payRequest,reqType,loginUser);
        }
        reqTxView.setAdapter(transactionRecyclerListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_request_transaction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            Intent intent = new Intent(RequestTransactionActivity.this, PayDashboard.class);
            if(reqType != null) {
                if(reqType.equals("PayMe")) {
                    intent.putExtra("tabNo",0);
                } else if(reqType.equals("PayU")) {
                    intent.putExtra("tabNo",1);
                } else {
                    intent.putExtra("tabNo",2);
                }
            }
            RequestTransactionActivity.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
