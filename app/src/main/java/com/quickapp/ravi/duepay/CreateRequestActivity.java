package com.quickapp.ravi.duepay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.quickapp.ravi.Data.PayRequestData;
import com.quickapp.ravi.adapter.TransactionRecyclerListAdapter;
import com.quickapp.ravi.model.PayRequest;


public class CreateRequestActivity  extends AppCompatActivity {

    private TransactionRecyclerListAdapter transactionRecyclerListAdapter;
    private RecyclerView reqTxView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_request);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        PayRequest payRequest = PayRequestData.getPayRequests().get(1);

        // Initialize recycler view
        reqTxView = (RecyclerView) findViewById(R.id.addReqTxRecyclerView);
        reqTxView.setLayoutManager(new LinearLayoutManager(this));
        transactionRecyclerListAdapter = new TransactionRecyclerListAdapter(CreateRequestActivity.this,
                payRequest.transactionList,payRequest,null,null);
        reqTxView.setAdapter(transactionRecyclerListAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_request, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            Intent intent = new Intent(CreateRequestActivity.this, PayDashboard.class);
            CreateRequestActivity.this.startActivity(intent);
        }

        if (id == R.id.action_add) {
            Intent intent = new Intent(CreateRequestActivity.this, CreateRequestTransactionActivity.class);
            CreateRequestActivity.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
