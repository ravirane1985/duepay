package com.quickapp.ravi.model;

import java.io.Serializable;

/**
 * Created by ravirane on 10/30/15.
 */
public class CreditCard  implements Serializable {

    public String cardNumber;
    public String cardName;
    public String bankName;
    public String cardExpiry;
    public String cvv;
}
