package com.quickapp.ravi.model;

import java.io.Serializable;

/**
 * Created by ravirane on 10/27/15.
 */
public class Transaction implements Serializable {

    public int transactionId;
    public int requestId;
    public User senderUser;
    public String senderCardNo;
    public String amount;
    public String senderNote;
    public String txStartDate;
    public User receiverUser;
    public String receiverCardNo;
    public String receiverNote;
    public String txStatus; /*Initiated,Inprogress,Accepted,Rejected*/
    public String txCompletionDate;
    public String currency;
    public String txType;
    public String payStatus;
    public String payResponse;

    public Transaction() {
    }

    public Transaction(int transactionId, User senderUser, String senderCardNo, String amount, String senderNote, String txStartDate, User receiverUser, String receiverCardNo, String receiverNote, String txStatus, String txCompletionDate, String currency) {
        this.transactionId = transactionId;
        this.senderUser = senderUser;
        this.senderCardNo = senderCardNo;
        this.amount = amount;
        this.senderNote = senderNote;
        this.txStartDate = txStartDate;
        this.receiverUser = receiverUser;
        this.receiverCardNo = receiverCardNo;
        this.receiverNote = receiverNote;
        this.txStatus = txStatus;
        this.txCompletionDate = txCompletionDate;
        this.currency = currency;
    }
}
