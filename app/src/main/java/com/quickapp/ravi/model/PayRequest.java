package com.quickapp.ravi.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by ravirane on 10/28/15.
 */
public class PayRequest implements Serializable {

    public int requestId;
    public String requestTitle;
    public String requestType;
    public Date requestStartDate;
    public double requestAmount;
    public String requestCurrency;
    public String requestStatus;
    public Date requestCompletionDate;
    public List<Transaction> transactionList;
    public User requestUser;

    public PayRequest(int requestId, String requestTitle, String requestType, Date requestStartDate, double requestAmount, String requestCurrency, String requestStatus, Date requestCompletionDate, List<Transaction> transactionList, User requestUser) {
        this.requestId = requestId;
        this.requestTitle = requestTitle;
        this.requestType = requestType;
        this.requestStartDate = requestStartDate;
        this.requestAmount = requestAmount;
        this.requestCurrency = requestCurrency;
        this.requestStatus = requestStatus;
        this.requestCompletionDate = requestCompletionDate;
        this.transactionList = transactionList;
        this.requestUser = requestUser;
    }
}
