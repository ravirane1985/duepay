package com.quickapp.ravi.model;

import java.io.Serializable;

/**
 * Created by ravirane on 10/27/15.
 */
public class User implements Serializable {
    public int uId;
    public String uName;
    public String country;
    public String countryCode;
    public String contactNo;
    public String mailId;
    public boolean emailVerified;
    public boolean phoneVerified;

    public User() {
    }

    public User(int uId, String uName, String contactNo) {
        this.uId = uId;
        this.uName = uName;
        this.contactNo = contactNo;
    }
}
